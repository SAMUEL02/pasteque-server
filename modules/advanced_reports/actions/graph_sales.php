<?php
//    Pastèque Web back office, Products module
//
//    Copyright (C) 2016 Scil (http://scil.coop)
//        Philippe Pary philippe@scil.coop
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace AdvancedReports;

// Date limitation
if(!isset($_POST["start"])) {
	$_POST["start"] = date("d/m/Y", strtotime("-7 days"));
}
if(!isset($_POST["stop"])) {
	$_POST["stop"] = date("d/m/Y");
}
$form .= \Pasteque\row(\Pasteque\form_date("start", $_POST["start"]));
$form .= \Pasteque\row(\Pasteque\form_date("stop", $_POST["stop"]));
$form .= \Pasteque\form_button("Envoyer");
echo \Pasteque\form_generate(\Pasteque\get_current_url(),"POST",$form);

$cashes = \Pasteque\CashesService::getAll();
$start = \DateTime::createFromFormat("d/m/Y",$_POST["start"]);
$stop = \DateTime::createFromFormat("d/m/Y",$_POST["stop"]);

foreach($cashes as $cash) {
	$openDate = \DateTime::createFromFormat("U",$cash->openDate);
	$closeDate = \DateTime::createFromFormat("U",$cash->closeDate);
	if((is_bool($openDate) || is_bool($closeDate)) || ($closeDate < $start || $openDate > $stop)) {
		continue;
	}
	$cashRegister = \Pasteque\CashRegistersService::getFromCashId($cash->id);
	$labels[] =  $openDate->format("d/m/Y H:i") . "-" . $closeDate->format("d/m/Y H:i") . " (" . $cashRegister->label . ", " . $cash->sequence . ")";
	$data[] = round($cash->total,2);
}
?>
<script src="lib/chartjs/Chart.min.js"></script>
<canvas id="myChart" width="400" height="400"></canvas>
<script>
var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: <?=json_encode($labels)?>,
        datasets: [{
            label: '<?php \pi18n("Sales with VAT", PLUGIN_NAME)?>',
            data: <?=json_encode($data)?>,
	    fill: true,
	    lineTension: 0.1,
	    borderColor: "#A2351E",
	    backgroundColor: "#D5B66E",
        }]
    },
    options: {
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero:false
                }
            }],
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

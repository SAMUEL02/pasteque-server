<?php
//    Pastèque API
//
//    Copyright (C) 2012-2016 Scil (http://scil.coop)
//    Cédric Houbart, Philippe Pary
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace BaseProducts;

// open csv return null if the file selected had not extension "csv"
// or user not selected file
function init_csv() {
	if ($_FILES['csv']['tmp_name'] === NULL) {
		return NULL;
	}
	$ext = strchr($_FILES['csv']['type'], "/");
	$ext = strtolower($ext);

	if($ext !== "/csv" && $ext !== "/plain") {
		return NULL;
	}

	$key = array('label', 'reference', 'parent', 'dispOrder');

	$optionKey = array('image');
	
	$csv = new \Pasteque\Csv($_FILES['csv']['tmp_name'], $key, $optionKey);

	if (!$csv->open()) {
		return $csv;
	}

	//manage empty string
	$csv->setEmptyStringValue("disp_order", null);
	$csv->setEmptyStringValue("image", null);
	
	return $csv;
}

function import_csv($csv, $verbose) {
	$error_mess = array();
	$update = 0;
	$create = 0;
	$error=0;
	$created = "";
	$updated = "";

	while ($tab = $csv->readLine()) {
		$parentOk = false;
		if ($tab['parent'] !== NULL) {
			$parent = \Pasteque\CategoriesService::getByName($tab['parent']);
			if ($parent) {
				$parentOk = true;
				$tab['parent'] = $parent->id;
			}
		} else {
			// Category isn't subCategory
			$parentOk = true;
		}

		if ($parentOk) {
			$cat = new \Pasteque\Category($tab['reference'], $tab['parent'], $tab['label'],
					null, $tab['dispOrder']);
			$category_exist = \Pasteque\CategoriesService::getByName($cat->label);
			if ($category_exist) {
				//UPDATE category
				if($tab['image'] != null) {
					if($tab['image'] != "#") {
						$img = hex2bin($tab['image']);
					} else {
						$img = null;
					}
				} else {
					$img = \Pasteque\PT::IMG_KEEP;
				}
				$cat->id = $category_exist->id;
				if (\Pasteque\CategoriesService::updateCat($cat, $img)) {
					$update++;
					if ($verbose) {
						if ($updated != "") {
							$updated .= ", ";
						}
						$updated .= $tab['label']."@".$csv->getCurrentLineNumber();
					}
				} else {
					$error++;
					$error_mess[] = \i18n("On line %d: Cannot update category: '%s'", PLUGIN_NAME,
							$csv->getCurrentLineNumber(), $tab['label']);
				}
			} else {
				//CREATE category
				if(($tab['image'] != null) && ($tab['image'] != "#")) {
					$img = hex2bin($tab['image']);
				} else {
					$img = null;
				}
				$id = \Pasteque\CategoriesService::createCat($cat, $img);
				if ($id) {
					$create++;
					if ($verbose) {
						if ($created != "") {
							$created .= ", ";
						}
						$created .= $tab['label']."@".$csv->getCurrentLineNumber();
					}
				} else {
					$error++;
					$error_mess[] = \i18n("On line %d: Cannot create category: '%s'", PLUGIN_NAME,
							$csv->getCurrentLineNumber(), $tab['label']);
				}
			}
		} else {
			$error++;
			$error_mess[] = \i18n("On line %d: Category parent doesn't exist",
					PLUGIN_NAME, $csv->getCurrentLineNumber());
		}
	}

	if ($created != "") {
		if ($create > 1) {
			$error_mess[] = \i18n("Created categories: [%s]", PLUGIN_NAME, $created);
		} else {
			$error_mess[] = \i18n("Created category: %s", PLUGIN_NAME, $created);
		}
	}
	if ($updated != "") {
		if ($update > 1) {
			$error_mess[] = \i18n("Modified categories: [%s]", PLUGIN_NAME, $updated);
		} else {
			$error_mess[] = \i18n("Modified category: %s", PLUGIN_NAME, $updated);
		}
	}
	$message = \i18n("%d line(s) inserted, %d line(s) modified, %d error(s)",
			PLUGIN_NAME, $create, $update, $error );

	$csv->close();
	return array($message, $error_mess);
}

$error = null;
$message = null;
if (isset($_FILES['csv'])) {
	$dateStr = isset($_POST['date']) ? $_POST['date'] : \i18nDate(time());
	$dateStr = \i18nRevDate($dateStr);
	$date = \Pasteque\stdstrftime($dateStr);

	$csv = init_csv();
	if ($csv === NULL) {
		$error = \i18n("Selected file empty or bad format", PLUGIN_NAME);
	} else if (!$csv->isOpen()) {
		$err = array();
		foreach ($csv->getErrors() as $mess) {
			$err[] = \i18n($mess);
		}
		$error = $err;
	} else {
		$msgs = import_csv($csv, isset($_POST['verbose']), $date);
		$message = $msgs[0];
		$error = $msgs[1];
	}
}

echo \Pasteque\mainTitle(\i18n("Import categories from csv file", PLUGIN_NAME));
\Pasteque\tpl_msg_box($message, $error);
$content = \Pasteque\form_file("csv","csv",\i18n("File", PLUGIN_NAME));
$content .= \Pasteque\form_input("csv","csv",$verbose, "verbose", "boolean", array("default" => isset($_POST['verbose'])));
$content .= \Pasteque\form_send();
echo \Pasteque\form_generate(\Pasteque\get_current_url(), "post", $content);

?>

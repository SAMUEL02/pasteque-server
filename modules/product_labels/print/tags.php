<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ProductLabels;

Define("FPDF_FONTPATH", dirname(__FILE__)."/fonts/");

if(isset($_POST["format"]))
    $formatFile = "modules/product_labels/print/templates/".$_POST["format"].".php";

if(isset($formatFile) && file_exists($formatFile)) {
    require_once($formatFile);
}
else {
    // default is A4, first format implemented
    Define("PAPER_SIZE","A4");
    Define("PAPER_ORIENTATION","P");
    
    Define("ROW_SIZE", 20);
    Define("COL_SIZE", 50);
    Define("ROW_NUM", 14);
    Define("COL_NUM", 4);
    Define("V_MARGIN", 5);
    Define("H_MARGIN", 5);
    Define("V_PADDING", 0);
    Define("H_PADDING", 0);
    
    Define("LABEL_X", 0);
    Define("LABEL_Y", 0);
    Define("LABEL_WIDTH", 48);
    Define("LABEL_HEIGHT", 7);
    Define("LABEL_DOTS", 7);
    Define("LABEL_FRAME", 0);
    
    Define("BARCODE_X", 0);
    Define("BARCODE_Y", 7);
    Define("BARCODE_WIDTH", 24);
    Define("BARCODE_HEIGHT", 9);
    Define("BARCODE_ANGLE", 0);
    
    Define("BARCODE_TEXT_X", 0);
    Define("BARCODE_TEXT_Y", 16);
    Define("BARCODE_TEXT_HEIGHT", 4);
    Define("BARCODE_TEXT_DOTS", 6);
    Define("BARCODE_TEXT_FRAME", 0);
    
    Define("PRICE_X", 24);
    Define("PRICE_Y", 7);
    Define("PRICE_WIDTH", 24);
    Define("PRICE_HEIGHT", 6);
    Define("PRICE_DOTS", 14);
    Define("PRICE_FRAME", "LTR");
    
    Define("UNIT_X", 24);
    Define("UNIT_Y", 13);
    Define("UNIT_WIDTH", 24);
    Define("UNIT_HEIGHT", 3);
    Define("UNIT_DOTS", 6);
    Define("UNIT_FRAME", "LBR");

    Define("REF_X", 24);
    Define("REF_Y", 16);
    Define("REF_WIDTH", 24);
    Define("REF_HEIGHT", 4);
    Define("REF_DOTS", 6);
    Define("REF_FRAME", 0);
}

require_once(\Pasteque\PT::$ABSPATH . "/lib/barcode-master/php-barcode.php");

function conv_label($label) {
	return iconv("UTF-8", "ISO-8859-15//TRANSLIT", $label);
}

function pdf_label($pdf, $productId, $col, $row, $h_margin, $v_margin) {
    $currSrv = new \Pasteque\CurrenciesService();
	$product = \Pasteque\ProductsService::get($productId);
	$taxCat = \Pasteque\TaxesService::get($product->taxCatId);
	$tax = $taxCat->getCurrentTax();
	$vatprice = $product->priceSell * (1 + $tax->rate);
    $currency = $currSrv->getDefault();
	$price = i18nFlt($vatprice);
	$unit = "";
	
    $x = $h_margin + $col * COL_SIZE + $col * H_PADDING;
    $y = $v_margin + $row * ROW_SIZE + $row * V_PADDING;
    
    // Draw label limits
    $pdf->Line($x, $y, $x + 1, $y);
    $pdf->Line($x + COL_SIZE -1, $y, $x + COL_SIZE, $y);
    $pdf->Line($x + COL_SIZE, $y, $x + COL_SIZE, $y +1);
    $pdf->Line($x + COL_SIZE, $y + ROW_SIZE - 1, $x + COL_SIZE, $y +ROW_SIZE);
    $pdf->Line($x + COL_SIZE, $y + ROW_SIZE , $x + COL_SIZE -1, $y + ROW_SIZE);
    $pdf->Line($x + 1, $y + ROW_SIZE, $x, $y + ROW_SIZE);
    $pdf->Line($x, $y + ROW_SIZE, $x, $y + ROW_SIZE - 1);
    $pdf->Line($x, $y + 1, $x, $y);
    
    // Designation
    if ( LABEL_X >= 0) {
        $pdf->SetFont('TiresiasInfofont','',LABEL_DOTS);
        $pdf->SetXY($x + LABEL_X, $y + LABEL_Y);
        
    	$label = $product->label;
    	$maxwidth = floatval(LABEL_WIDTH);
    	if ($pdf->GetStringWidth($label) > $maxwidth) {
            // print label on  two lines. Try to cut on space
            $space = $pdf->GetStringWidth(' ');
    		$words = preg_split('/ +/', $label);
    		$text = "";
    		$width = 0;
    		$lines[] = array();
    		$lineId = 0;
    		
    		foreach ($words as $word) {
    			if ($width < $maxwidth) {
                    $wordwidth = $pdf->GetStringWidth($word);
                    if ($wordwidth > $maxwidth) {
                        // Word is too long, we cut it
                        for($i=0; $i<strlen($word); $i++) {
                            $wordwidth = $pdf->GetStringWidth(substr($word, $i, 1));
                            if($width + $wordwidth <= $maxwidth) {
                                $width += $wordwidth;
                                $text .= substr($word, $i, 1);
                            } else {
			    				$lines[$lineId++] = $text;
    							$text = "";
    							$width = 0;
                            }
                        }
                    } elseif ($width + $wordwidth <= $maxwidth) {
                    	// Add this word
                        $width += $wordwidth + $space;
                        $text .= $word.' ';
                    } else {
                    	// Too long to fit
	    				$lines[$lineId++] = $text;
                        $text = $word.' ';
						$width = $wordwidth + $space;
                    }
    			} else {
    				$lines[$lineId++] = $text;
    				$text = "";
    				$width = 0;
    			}
    			$count++;
            }
            if ($width > 0) {
            	$lines[$lineId++] = $text;
            }
            $nblines = count($lines);
            for ($i=0; $i<=$nblines; $i++) {
	   	        $pdf->SetXY($x + LABEL_X, $y + LABEL_Y+$i*LABEL_HEIGHT/$nblines);
   		        $pdf->Cell(LABEL_WIDTH, LABEL_HEIGHT/$nblines, conv_label($lines[$i]), LABEL_FRAME, 1, "C");
            }
   	    } else {
            $pdf->Cell(LABEL_WIDTH, LABEL_HEIGHT, conv_label($label), LABEL_FRAME, 1, "C");
        }
    }
    
    // Barcode
    if ( BARCODE_X >= 0) {
	    $pdf->SetXY($x + BARCODE_X, $y + BARCODE_Y);
	    if ($product->barcode == \BarcodeEAN::compute($product->barcode, "ean8")) {
	    	$format = "ean8";
	    	$lwidth = BARCODE_WIDTH / (15 * 7);
	    } else if ($product->barcode == \BarcodeEAN::compute($product->barcode, "ean13")) {
	    	$format = "ean13";
	    	$lwidth = BARCODE_WIDTH / (15 * 7);
	    } else {
	    	$format = "code128";
	    	$lwidth = BARCODE_WIDTH / (15 * 9);
	    }
	    $data = \Barcode::fpdf($pdf, 
	    		"000000",
	            $pdf->GetX() + BARCODE_WIDTH / 2, 
	    		$pdf->GetY() + BARCODE_HEIGHT / 2,
	            BARCODE_ANGLE, 
	    		$format, 
	    		array('code' => $product->barcode),
	            $lwidth, 
	    		BARCODE_HEIGHT);
    }
    
    if ( BARCODE_TEXT_X >= 0) {
	    $pdf->SetFontSize(BARCODE_TEXT_DOTS);
    	$pdf->SetXY($x + BARCODE_TEXT_X, $y + BARCODE_TEXT_Y);
    	$pdf->Cell(BARCODE_WIDTH, BARCODE_TEXT_HEIGHT, conv_label($product->barcode), BARCODE_TEXT_FRAME, 1, "C");
    }

    // Price
    if ( PRICE_X >= 0) {
    	$pdf->SetFontSize(PRICE_DOTS);
    	$pdf->SetXY($x + PRICE_X, $y + PRICE_Y);
    	$pdf->Cell(PRICE_WIDTH, PRICE_HEIGHT, conv_label($price.$currency->symbol), PRICE_FRAME, 1, "C");
    }

    // Price by unit
    if ( UNIT_X >= 0) {
    	if ($product->printunitprice == 1) {
    		$format = \i18n("Volum reference", PLUGIN_NAME);
    		$value = $vatprice / $product->quantity;
    		$price = i18nFlt($value);
    		$format = str_replace(":value", $price, $format);
    		$unit = str_replace(":currency", $currency->symbol, $format);
       	} else if ($product->printunitprice == 2) {
    		$format = \i18n("Weight reference", PLUGIN_NAME);
    		$value = $vatprice / $product->quantity;
			$price = i18nFlt($value);
        	$format = str_replace(":value", $price, $format);
        	$unit = str_replace(":currency", $currency->symbol, $format);
    	}
    	$pdf->SetFontSize(UNIT_DOTS);
    	$pdf->SetXY($x + UNIT_X, $y + UNIT_Y);
    	$pdf->Cell(UNIT_WIDTH, UNIT_HEIGHT, conv_label($unit), UNIT_FRAME, 1, "C");
    }
        
    // Reference
    if ( REF_X >= 0) {
    	$pdf->SetFontSize(REF_DOTS);
    	$pdf->SetXY($x + REF_X, $y + REF_Y);
    	$pdf->Cell(REF_WIDTH, REF_HEIGHT, conv_label($product->reference), REF_FRAME, 1, "C");
        }
}

$skip = $_POST['start_from'] - 1;
$h_margin = $_POST['h_margin'];
$v_margin = $_POST['v_margin'];

$pdf = new \FPDF(PAPER_ORIENTATION, "mm", PAPER_SIZE);
$pdf->setMargins($h_margin, $v_margin);
$pdf->setAutoPageBreak(false,$v_margin);
$pdf->AddPage();
$pdf->AddFont('TiresiasInfofont','','Tiresias Infofont.php');
$pdf->SetFont('TiresiasInfofont','',TEXT_SIZE);

$col += $skip;
$row = intVal(floor($col / COL_NUM));
$col %= COL_NUM;
$dh = H_MARGIN + $h_margin;
$dv = V_MARGIN + $v_margin;
foreach ($_POST as $key => $value) {
    if (substr($key, 0, 4) == "qty-") {
        $productId = substr($key, 4);
        pdf_label($pdf, $productId, $col, $row, $dh, $dv);
        $col++;
        if ($col == COL_NUM) {
            $row++;
            if ($row == ROW_NUM) {
                $pdf->addPage();
                $row = 0;
            }
            $col = 0;
        }
    }
}

$pdf->Output();

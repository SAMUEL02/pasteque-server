<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 13;
const H_MARGIN = 5;
const V_PADDING = 0;
const H_PADDING = 0;

const COL_SIZE = 50;
const ROW_SIZE = 25;
const COL_NUM = 4;
const ROW_NUM = 11;

const LABEL_X = 2;
const LABEL_Y = 0;
const LABEL_WIDTH = 46;
const LABEL_HEIGHT = 10;
const LABEL_DOTS = 10;
const LABEL_FRAME = 0;

const BARCODE_X = 2;
const BARCODE_Y = 10;
const BARCODE_WIDTH = 23;
const BARCODE_HEIGHT = 11;
const BARCODE_ANGLE = 0;

const BARCODE_TEXT_X = 2;
const BARCODE_TEXT_Y = 21;
const BARCODE_TEXT_HEIGHT = 4;
const BARCODE_TEXT_DOTS = 7;
const BARCODE_TEXT_FRAME = 0;

const PRICE_X = 25;
const PRICE_Y = 10;
const PRICE_WIDTH = 23;
const PRICE_HEIGHT = 7;
const PRICE_DOTS = 13;
const PRICE_FRAME = "LTR";

const UNIT_X = 25;
const UNIT_Y = 17;
const UNIT_WIDTH = 23;
const UNIT_HEIGHT = 4;
const UNIT_DOTS = 7;
const UNIT_FRAME = "LBR";

const REF_X = 25;
const REF_Y = 21;
const REF_WIDTH = 23;
const REF_HEIGHT = 4;
const REF_DOTS = 7;
const REF_FRAME = 0;
?>

<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 10;
const H_MARGIN = 10;
const V_PADDING = 0;
const H_PADDING = 0;

const COL_SIZE = 100;
const ROW_SIZE = 100;
const COL_NUM = 2;
const ROW_NUM = 2;

const LABEL_X = 0;
const LABEL_Y = 0;
const LABEL_WIDTH = 100;
const LABEL_HEIGHT = 30;
const LABEL_DOTS = 30;
const LABEL_FRAME = 0;

const BARCODE_X = 0;
const BARCODE_Y = 65;
const BARCODE_WIDTH = 100;
const BARCODE_HEIGHT = 20;
const BARCODE_ANGLE = 0;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 85;
const BARCODE_TEXT_HEIGHT = 10;
const BARCODE_TEXT_DOTS = 20;
const BARCODE_TEXT_FRAME = 0;

const PRICE_X = 0;
const PRICE_Y = 30;
const PRICE_WIDTH = 100;
const PRICE_HEIGHT = 24;
const PRICE_DOTS = 60;
const PRICE_FRAME = "LTR";

const UNIT_X = 0;
const UNIT_Y = 54;
const UNIT_WIDTH = 100;
const UNIT_HEIGHT = 10;
const UNIT_DOTS = 20;
const UNIT_FRAME = "LBR";

const REF_X = 0;
const REF_Y = 95;
const REF_WIDTH = 100;
const REF_HEIGHT = 5;
const REF_DOTS = 10;
const REF_FRAME = 0;
?>

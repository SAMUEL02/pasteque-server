<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A4";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 5;
const H_MARGIN = 5;
const V_PADDING = 0;
const H_PADDING = 0;

const ROW_SIZE = 20;
const COL_SIZE = 50;
const ROW_NUM = 14;
const COL_NUM = 4;

const LABEL_X = 0;
const LABEL_Y = 0;
const LABEL_WIDTH = 48;
const LABEL_HEIGHT = 7;
const LABEL_DOTS = 7;
const LABEL_FRAME = 0;

const BARCODE_X = 0;
const BARCODE_Y = 7;
const BARCODE_WIDTH = 24;
const BARCODE_HEIGHT = 9;
const BARCODE_ANGLE = 0;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 16;
const BARCODE_TEXT_HEIGHT = 4;
const BARCODE_TEXT_DOTS = 6;
const BARCODE_TEXT_FRAME = 0;

const PRICE_X = 24;
const PRICE_Y = 7;
const PRICE_WIDTH = 24;
const PRICE_HEIGHT = 6;
const PRICE_DOTS = 14;
const PRICE_FRAME = "LTR";

const UNIT_X = 24;
const UNIT_Y = 13;
const UNIT_WIDTH = 24;
const UNIT_HEIGHT = 3;
const UNIT_DOTS = 6;
const UNIT_FRAME = "LBR";

const REF_X = 24;
const REF_Y = 16;
const REF_WIDTH = 24;
const REF_HEIGHT = 4;
const REF_DOTS = 6;
const REF_FRAME = 0;
?>

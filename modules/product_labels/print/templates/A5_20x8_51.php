<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

const PAPER_SIZE = "A5";
const PAPER_ORIENTATION = "P";

const V_MARGIN = 7.1;
const H_MARGIN = 2.4;
const V_PADDING = 0;
const H_PADDING = 0;

const ROW_SIZE = 8;
const COL_SIZE = 20;
const ROW_NUM = 17;
const COL_NUM =7;

const LABEL_X = -0.1;
const LABEL_Y = 0;
const LABEL_WIDTH = 20;
const LABEL_HEIGHT = 3;
const LABEL_DOTS = 4;
const LABEL_FRAME = 0;

const BARCODE_X = -0.1;
const BARCODE_Y = 3;
const BARCODE_WIDTH = 19;
const BARCODE_HEIGHT = 3;
const BARCODE_ANGLE = 0;

const BARCODE_TEXT_X = 0;
const BARCODE_TEXT_Y = 6;
const BARCODE_TEXT_HEIGHT = 2;
const BARCODE_TEXT_DOTS = 4;
const BARCODE_TEXT_FRAME = 0;

const PRICE_X = 0.5;
const PRICE_Y = 0.5;
const PRICE_WIDTH = 19;
const PRICE_HEIGHT = 4;
const PRICE_DOTS = 10;
const PRICE_FRAME = "LTR";

const UNIT_X = 0.5;
const UNIT_Y = 4;
const UNIT_WIDTH = 19;
const UNIT_HEIGHT = 2;
const UNIT_DOTS = 4;
const UNIT_FRAME = "LBR";

const REF_X = -0.1;
const REF_Y = 6;
const REF_WIDTH = 10;
const REF_HEIGHT = 2;
const REF_DOTS = 3;
const REF_FRAME = 0;
?>

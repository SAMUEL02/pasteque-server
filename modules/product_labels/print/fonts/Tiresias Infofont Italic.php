<?php
//    Pastèque Web back office, Product labels module
//
//    Copyright (C) 2017 Philippe Corbes
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

$type = 'TrueType';
$name = 'TiresiasInfofontItalic';
$desc = array('Ascent'=>771,'Descent'=>-220,'CapHeight'=>771,'Flags'=>96,'FontBBox'=>'[-164 -249 1221 986]','ItalicAngle'=>-11,'StemV'=>70,'MissingWidth'=>500);
$up = -133;
$ut = 500;
$cw = array(
	chr(0)=>500,chr(1)=>500,chr(2)=>500,chr(3)=>500,chr(4)=>500,chr(5)=>500,chr(6)=>500,chr(7)=>500,chr(8)=>500,chr(9)=>500,chr(10)=>500,chr(11)=>500,chr(12)=>500,chr(13)=>500,chr(14)=>500,chr(15)=>500,chr(16)=>500,chr(17)=>500,chr(18)=>500,chr(19)=>500,chr(20)=>500,chr(21)=>500,
	chr(22)=>500,chr(23)=>500,chr(24)=>500,chr(25)=>500,chr(26)=>500,chr(27)=>500,chr(28)=>500,chr(29)=>500,chr(30)=>500,chr(31)=>500,' '=>293,'!'=>338,'"'=>448,'#'=>883,'$'=>605,'%'=>879,'&'=>740,'\''=>264,'('=>454,')'=>454,'*'=>432,'+'=>652,
	','=>300,'-'=>411,'.'=>300,'/'=>422,'0'=>652,'1'=>427,'2'=>542,'3'=>544,'4'=>627,'5'=>556,'6'=>607,'7'=>567,'8'=>637,'9'=>607,':'=>300,';'=>300,'<'=>652,'='=>652,'>'=>652,'?'=>516,'@'=>979,'A'=>662,
	'B'=>652,'C'=>562,'D'=>676,'E'=>570,'F'=>547,'G'=>659,'H'=>704,'I'=>392,'J'=>336,'K'=>633,'L'=>515,'M'=>891,'N'=>745,'O'=>718,'P'=>619,'Q'=>718,'R'=>655,'S'=>599,'T'=>563,'U'=>702,'V'=>662,'W'=>904,
	'X'=>632,'Y'=>616,'Z'=>566,'['=>453,'\\'=>422,']'=>453,'^'=>676,'_'=>500,'`'=>522,'a'=>550,'b'=>589,'c'=>520,'d'=>589,'e'=>563,'f'=>382,'g'=>590,'h'=>596,'i'=>292,'j'=>292,'k'=>552,'l'=>316,'m'=>906,
	'n'=>596,'o'=>564,'p'=>589,'q'=>589,'r'=>399,'s'=>490,'t'=>395,'u'=>596,'v'=>530,'w'=>749,'x'=>556,'y'=>530,'z'=>478,'{'=>378,'|'=>260,'}'=>378,'~'=>730,chr(127)=>500,chr(128)=>500,chr(129)=>500,chr(130)=>500,chr(131)=>500,
	chr(132)=>500,chr(133)=>500,chr(134)=>500,chr(135)=>500,chr(136)=>500,chr(137)=>500,chr(138)=>500,chr(139)=>500,chr(140)=>500,chr(141)=>500,chr(142)=>500,chr(143)=>500,chr(144)=>500,chr(145)=>500,chr(146)=>500,chr(147)=>500,chr(148)=>500,chr(149)=>500,chr(150)=>500,chr(151)=>500,chr(152)=>500,chr(153)=>500,
	chr(154)=>500,chr(155)=>500,chr(156)=>500,chr(157)=>500,chr(158)=>500,chr(159)=>500,chr(160)=>293,chr(161)=>338,chr(162)=>569,chr(163)=>585,chr(164)=>668,chr(165)=>654,chr(166)=>500,chr(167)=>500,chr(168)=>500,chr(169)=>885,chr(170)=>500,chr(171)=>536,chr(172)=>653,chr(173)=>652,chr(174)=>885,chr(175)=>500,
	chr(176)=>427,chr(177)=>500,chr(178)=>500,chr(179)=>500,chr(180)=>500,chr(181)=>500,chr(182)=>709,chr(183)=>298,chr(184)=>500,chr(185)=>500,chr(186)=>500,chr(187)=>536,chr(188)=>912,chr(189)=>900,chr(190)=>616,chr(191)=>516,chr(192)=>662,chr(193)=>662,chr(194)=>662,chr(195)=>662,chr(196)=>662,chr(197)=>662,
	chr(198)=>941,chr(199)=>562,chr(200)=>570,chr(201)=>570,chr(202)=>570,chr(203)=>570,chr(204)=>392,chr(205)=>392,chr(206)=>392,chr(207)=>392,chr(208)=>500,chr(209)=>745,chr(210)=>718,chr(211)=>718,chr(212)=>718,chr(213)=>718,chr(214)=>718,chr(215)=>652,chr(216)=>718,chr(217)=>702,chr(218)=>702,chr(219)=>702,
	chr(220)=>702,chr(221)=>616,chr(222)=>500,chr(223)=>656,chr(224)=>550,chr(225)=>550,chr(226)=>550,chr(227)=>550,chr(228)=>550,chr(229)=>550,chr(230)=>858,chr(231)=>478,chr(232)=>563,chr(233)=>563,chr(234)=>563,chr(235)=>563,chr(236)=>292,chr(237)=>292,chr(238)=>292,chr(239)=>292,chr(240)=>500,chr(241)=>596,
	chr(242)=>564,chr(243)=>564,chr(244)=>564,chr(245)=>564,chr(246)=>564,chr(247)=>652,chr(248)=>564,chr(249)=>596,chr(250)=>596,chr(251)=>596,chr(252)=>596,chr(253)=>530,chr(254)=>500,chr(255)=>530);
$enc = 'ISO-8859-15';
$diff = '128 /.notdef 130 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 142 /.notdef 145 /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef /.notdef 158 /.notdef /.notdef 164 /Euro 166 /Scaron 168 /scaron 180 /Zcaron 184 /zcaron 188 /OE /oe /Ydieresis';
$uv = array(0=>array(0,164),164=>8364,165=>165,166=>352,167=>167,168=>353,169=>array(169,11),180=>381,181=>array(181,3),184=>382,185=>array(185,3),188=>array(338,2),190=>376,191=>array(191,65));
$file = 'Tiresias Infofont Italic.z';
$originalsize = 30108;
$subsetted = true;
?>

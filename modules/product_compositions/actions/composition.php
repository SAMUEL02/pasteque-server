<?php
//    Pastèque Web back office, Products module
//
//    Copyright (C) 2013-2016 Scil (http://scil.coop)
//        Cédric Houbart, Philippe Pary philippe@scil.coop
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace ProductCompositions;

$message = NULL;
$error = NULL;
if (isset($_GET['delete-comp'])) {
	if (\Pasteque\CompositionsService::delete($_GET['delete-comp'])) {
		$message = \i18n("Changes saved");
	} else {
		$error = \i18n("Unable to save changes");
	}
}

$compositions = \Pasteque\CompositionsService::getAll();
$categories = \Pasteque\CategoriesService::getAll();
$cmpCat = array();
$archivesCat = array();
foreach ($compositions as $cmp) {
    $cmpCat[$cmp->categoryId][] = $cmp;
	// Archive will be filled on display loop
}

//Title
echo \Pasteque\row(\Pasteque\mainTitle(\i18n("Compositions", PLUGIN_NAME)));
//Buttons
$buttons = \Pasteque\addButton(\i18n("Add composition", PLUGIN_NAME),\Pasteque\get_module_url_action(PLUGIN_NAME, "composition_edit"));
echo \Pasteque\row(\Pasteque\buttonGroup($buttons));
//Information
\Pasteque\tpl_msg_box($message, $error); 
//Counter
echo \Pasteque\row(\Pasteque\counterDiv(\i18n("%d compositions", PLUGIN_NAME, count($compositions))));

if (count($compositions) == 0) {
	echo \Pasteque\errorDiv(\i18n("No composition found", PLUGIN_NAME));
}
else {
    $content = array();
    $archive = false;
    foreach ($categories as $category) {
        if (isset($cmpCat[$category->id])) {
            $content[] = array();
            $content[0][0] = \i18n("Composition.label");
            $content[0][1] = \i18n("Product.reference");
            $content[0][2] = \i18n("Product.label");
            $i = 1;
            foreach ($cmpCat[$category->id] as $product) {
                if ($product->visible) {
                    if ($product->hasImage) {
                        $imgSrc = \Pasteque\PT::URL_ACTION_PARAM . "=img&w=product&id=" . $product->id;
                    } else {
                        $imgSrc = \Pasteque\PT::URL_ACTION_PARAM . "=img&w=product";
                    }
                    $content[$i][0] = "<img class=\"img img-thumbnail thumbnail\" src=\"?" . $imgSrc . "\">";
                    $content[$i][1] = $product->reference;
                    $content[$i][2] = $product->label;
                    $btn_group = \Pasteque\editButton(\i18n('Edit', PLUGIN_NAME), \Pasteque\get_module_url_action(PLUGIN_NAME, 'composition_edit', array("id" => $product->id)));
                    $btn_group .= \Pasteque\deleteButton(\i18n('Delete', PLUGIN_NAME), \Pasteque\get_current_url() . "&delete-comp=" . $product->id);
                    $content[$i][2] .= \Pasteque\buttonGroup($btn_group, "pull-right");
                    $i++;
                }
                else {
                    $archive = true;
                    $archivesCat[$category->id][] = $product;
                }
            }
            if(sizeof($content) > 1) {
                echo \Pasteque\row(\Pasteque\secondaryTitle(\Pasteque\esc_html($category->label)));
                echo \Pasteque\row(\Pasteque\standardTable($content));
            }
            unset($content);
        }
    }
    if ($archive) {
        foreach ($categories as $category) {
            $content[0][0] = "";
            $content[0][1] = \i18n("Product.reference");
            $content[0][2] = \i18n("Product.label");
            $i = 1;
            if (isset($archivesCat[$category->id])) {
                foreach ($archivesCat[$category->id] as $product) {
                    if (!$product->visible) {
                        if ($product->hasImage) {
                            $imgSrc = \Pasteque\PT::URL_ACTION_PARAM . "=img&w=product&id=" . $product->id;
                        } else {
                            $imgSrc = \Pasteque\PT::URL_ACTION_PARAM . "=img&w=product";
                        }
                        $content[$i][0] = "<img class=\"img img-thumbnail thumbnail\" src=\"?" . $imgSrc . "\">";
                        $content[$i][1] = $product->reference;
                        $content[$i][2] = $product->label;
                        $btn_group = \Pasteque\editButton(\i18n('Edit', PLUGIN_NAME), \Pasteque\get_module_url_action(PLUGIN_NAME, 'composition_edit', array("id" => $product->id)));
                        $content[$i][2] .= \Pasteque\buttonGroup($btn_group, "pull-right");
                        $i++;
                    }
                }
            }
            if(sizeof($content) > 1) {
                echo \Pasteque\row(\Pasteque\secondaryTitle(\Pasteque\esc_html($category->label) . "&nbsp;-&nbsp;" . \i18n("Archived", PLUGIN_NAME)));
                echo \Pasteque\row(\Pasteque\standardTable($content));
            }
            unset($content);
        }
    }

}

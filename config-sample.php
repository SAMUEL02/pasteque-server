<?php
//    Pastèque Web back office, general configuration
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

//////////////////
//  Debug mode  //
//////////////////

$config['debug'] = false;
// Uncomment to add an other log directory. System log is still enabled.
//$config['log_dir'] = "/var/log/pasteque";
// Log level: one of DEBUG, INFO, WARNING, ERROR
$config['log_level'] = 'WARNING';

//////////////////
// Core modules //
//////////////////
// Don't forget to set the configuration file in each module if it has one.
// The values must match a directory in the core module directory
// I.e. core_ident must match a directory under core_modules/ident/

$config['core_ident'] = 'inifile';
$config['core_database'] = 'inifile';

// Template
// Must match a directory in templates/
// Warning: in version 7, only pasteque-bootstrap is working
$config['template'] = 'pasteque-bootstrap';

// Thumbnail size in pixels
$config['thumb_width'] = 128;
$config['thumb_height'] = 128;
$config['thumb_quality'] = 50;

// JWT auth
$config['jwt_timeout'] = 600; // Validity in seconds of an auth token
$config['jwt_secret'] = 'Change this to a superstrong password';

// Allow CORS for ajax clients from other domains.
// Can be a single value like '*' (not recommended)
// or an array of hosts with protocol.
// If disabled, accepts only requests from the same domain
//$config['allowed_origin'] = ['https://my.client.com', 'https://other.domain.com'];

// Paypal config (for module payment)
$config['pp_sandbox_id'] = '';
$config['pp_user_id'] = '';
$config['pp_email'] = '';
$config['pp_sandbox_email'] = '';
$config['pp_sandbox'] = true;

$config['pp_modules'] = [
    array('module' => 'product_barcodes', 'price' => '1.00'),
];
$config['mandatory_modules'] = [
    'base_products',
    'base_sales',
    'modules_management',
];
$config['free_modules'] = [
    'base_restaurant',
    'base_cashes',
    'base_resources',
    'base_stocks',
    'base_users',
    'product_compositions',
    'product_discounts',
];

function getConfig() {
    global $config;
    return $config;
}

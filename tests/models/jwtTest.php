<?php
//    Pasteque server testing
//
//    Copyright (C) 2012 Scil (http://scil.coop)
//
//    This file is part of Pasteque.
//
//    Pasteque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pasteque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pasteque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

require_once(dirname(dirname(__FILE__)) . "/common_load.php");

class JWTTest extends \PHPUnit_Framework_TestCase {

    public static function setUpBeforeClass() {
    }

    protected function tearDown() {
    }

    public static function tearDownAfterClass() {
    }

    
    public function testBuild() {
        $header = array("alg" => "HS512");
        $payload = array("test" => "content");
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertArrayHasKey("alg", $jwt->header,
                "alg key not found in header.");
        $this->assertEquals("HS512", $jwt->header["alg"],
                "alg key alterated in header.");
        $this->assertArrayHasKey("test", $jwt->payload,
                "test key not found in header.");
        $this->assertEquals("content", $jwt->payload["test"],
                "test key alterated in payload.");
        $this->assertEquals("eyJhbGciOiJIUzUxMiJ9.eyJ0ZXN0IjoiY29udGVudCJ9.93f931b941abe24c3435bbbf5a0494b2a20a48d1fe45c81ed1039f290d40b971d92470bbd1a82923f0262a9937f1c945af8a57ff1ea0fd3e6028caf54227c168", $jwt->token,
                "Invalid token value");
    }

    public function testDecode() {
        $token = "eyJhbGciOiJIUzUxMiJ9.eyJ0ZXN0IjoiY29udGVudCJ9.93f931b941abe24c3435bbbf5a0494b2a20a48d1fe45c81ed1039f290d40b971d92470bbd1a82923f0262a9937f1c945af8a57ff1ea0fd3e6028caf54227c168"; // see above
        $secret = "auienstldbepoldt";
        $jwt = JWT::decode($token, $secret);
        $this->assertFalse($jwt === null, "Unable to decode a valid token.");
        $this->assertEquals("HS512", $jwt->header["alg"]);
        $this->assertEquals("content", $jwt->payload["test"]);
    }

    /** @depends testBuild
     * @depends testDecode */
    public function testIsValidReject() {
        $header = array("alg" => "HS512");
        $payload = array("test" => "content");
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertFalse($jwt->isValid(100));
    }
    
    /** @depends testBuild
     * @depends testDecode */
    public function testIsValid() {
        $header = array("alg" => "HS512");
        $now = time() - 10; // Considered issued 10 seconds before
        $payload = array("iat" => $now);
        $secret = "auienstldbepoldt";
        $jwt = JWT::build($header, $payload, $secret);
        $this->assertTrue($jwt->isValid(20));
        $this->assertFalse($jwt->isValid(5));
    }
}
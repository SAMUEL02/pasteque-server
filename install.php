<?php
//    Pastèque Web back office
//
//    Copyright (C) 2017 Philippe Pary <philippe@pasteque.org>
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.
namespace Pasteque;

// Wizard page for generating config.php and database access.

require_once(__DIR__ . '/inc/constants.php');

$config['template'] = 'pasteque-bootstrap';
PT::$ABSPATH = __DIR__; // Base path. Also to check if a call

require_once(PT::$ABSPATH . "/inc/Log.php");
require_once(PT::$ABSPATH . "/inc/date_utils.php");
require_once(PT::$ABSPATH . "/inc/url_broker.php");
require_once(PT::$ABSPATH . "/inc/i18n.php");
require_once(PT::$ABSPATH . "/inc/i18n_aliases.php");
require_once(PT::$ABSPATH . "/inc/Report.php");
require_once(PT::$ABSPATH . "/inc/hooks.php");
require_once(PT::$ABSPATH . "/inc/PDOBuilder.php");
require_once(PT::$ABSPATH . "/inc/DB.php");
require_once(PT::$ABSPATH . "/inc/images.php");
require_once(PT::$ABSPATH . "/inc/Module.php");
require_once(PT::$ABSPATH . "/inc/jwt.php");
require_once(PT::$ABSPATH . "/inc/login.php");
require_once(PT::$ABSPATH . '/inc/load_template.php');
require_once(PT::$ABSPATH . '/inc/installer.php');


tpl_open();

if (file_exists('config.php')) {
  echo "<div class='alert'><p class='bg-danger'>&nbsp;Config file <em>config.php</em> already exists. Nothing will be done by this script as long at this file exists</p></div>";
}
elseif (isset($_POST["dbuser"])) {
  // Step zero : security and default values
  $arguments = Installer::sanitize_n_default($_POST);
  if($arguments === false) {
    $error = "This is a WTF";
  }

  // First step : system checks (FS, DB server)
  if(!isset($error)) {
    $checks_result = Installer::perform_checks();
    if($checks_result !== 1) {
      $error = $checks_result;
      echo \Pasteque\errorDiv($error);
    }
  }

  // Second step : try to install database
  if(!isset($error)) {
    $dbsetup_result = Installer::database_setup();
    if ($dbsetup_result !== 1) {
      $error = $dbsetup_result;
      echo \Pasteque\errorDiv($error);
    }
  }

  // Third step : try to create user config files
  if(!isset($error)) {
    $userconfig_result = Installer::userconfig_write();
    if ($userconfig_result !== 1) {
      $error = $userconfig_result;
      echo \Pasteque\errorDiv($error);
    }
  }

  // Last step : try to create config.php
  if(!isset($error)) {
    $configfile_result = Installer::configfile_write();
    if ($configfile_result !== 1) {
      $error = $configfile_result;
      echo \Pasteque\errorDiv($error);
    }
  }

  if (!isset($error)) {
    echo "
      <div class=\"page-header text-center\">
        <h1>Installation of Pasteque-server</h1>
      </div>";
    echo \Pasteque\infoDiv(
      sprintf("
      You can now connect with these credentials:
      <ul>
        <li>Login: %s</li>
        <li>Password: %s (it's the last time you get it clear, remember it !)</li>
      </ul>",$arguments["username"],$arguments["userpassword"]));
  }
  else {
    include(PT::$ABSPATH . '/templates/' . $config['template'] . '/install.php');
  }
}
else {
  include(PT::$ABSPATH . '/templates/' . $config['template'] . '/install.php');
}

tpl_close();


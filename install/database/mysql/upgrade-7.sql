--    Pasteque is a point of sales software
--    Copyright (C) 2017 SARL SCOP Scil
--    http://github.com/ScilCoop
--
--    This file is part of Pasteque
--
--    POS-tech is free software: you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation, either version 3 of the License, or
--    (at your option) any later version.
--
--    POS-tech is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with POS-tech. If not, see <http://www.gnu.org/licenses/>.

-- Database upgrade script for MYSQL

-- db v7 - v8

-- final script


-- Products: Add 2 Columns for labels
ALTER TABLE `pasteque`.`PRODUCTS` ADD COLUMN `PRINTUNITPRICE`  INT(11) NOT NULL DEFAULT '0' AFTER `ISPREPAY` , ADD COLUMN `QUANTITY` DOUBLE NOT NULL DEFAULT '1.0'  AFTER `PRINTUNITPRICE` ;

-- App version
UPDATE APPLICATIONS SET ID = "pasteque", NAME = "Pasteque", VERSION = 8 WHERE ID = "postech" OR ID = "pasteque";

-- Change SHAREDTICKETLINES's QUANTITY column type int to real
ALTER TABLE `pasteque`.`SHAREDTICKETLINES` MODIFY `QUANTITY` DOUBLE NOT NULL;

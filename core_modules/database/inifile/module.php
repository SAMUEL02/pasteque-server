<?php
//    Pastèque Web back office, Ini File database module
//
//    Copyright (C) 2013 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

// Read database data from ini files.

namespace IniFileDb;

require_once(__DIR__ . '/config.php');

$database = null;

function getDatabase($login) {
    global $config;
    if (substr($config['path'], 0, 1) == '.') {
        $dir = \Pasteque\PT::$ABSPATH . '/' . $config['path'];
    } else {
        $dir = $config['path'];
    }
    $sanitizedUser = str_replace('.', '_dot_', $login);
    $sanitizedUser = str_replace('/', '_slh_', $sanitizedUser);
    $sanitizedUser = str_replace('\\', '_aslh_', $sanitizedUser);
    $sanitizedUser = str_replace(' ', '_sp_', $sanitizedUser);
    $file = $dir . '/' . $sanitizedUser . '_db.ini';
    if (is_readable($file)) {
        $data = parse_ini_file($file);
        // Mandatory values
        if (empty($data['type']) || empty($data['host'])) { return false; }
        // Set default values
        if (empty($data['port'])) {
            switch ($data['type']) {
            case 'mysql': $data['port'] = 3306; break;
            case 'postgresql': $data['port'] = 5432; break;
            default: return false;
            }
        }
        if (empty($data['name'])) { $data['name'] = $sanitizedUser; }
        if (empty($data['user'])) { $data['user'] = $sanitizedUser; }
        if (empty($data['password'])) { $data['password'] = null; }
        return array('type' => $data['type'], 'host' => $data['host'],
                'port' => $data['port'], 'name' => $data['name'],
                'user' => $data['user'], 'password' => $data['password']);
    } else {
        // No ini file found or not readable
        return false;
    }
}

function get($login, $prop) {
    global $database;
    if ($database !== null) { return $database[$prop]; }
    else {
        $db = getDatabase($login);
        if ($db !== false) {
            $database = $db;
            return $database[$prop];
        }
    }
    return null;
}

namespace Pasteque;

function get_db_type($userId) {
    return \IniFileDb\get($userId, 'type');
}
function get_db_host($userId) {
    return \IniFileDb\get($userId, 'host');
}

function get_db_port($userId) {
    return \IniFileDb\get($userId, 'port');
}

function get_db_name($userId) {
    return \IniFileDb\get($userId, 'name');
}

function get_db_user($userId) {
    return \IniFileDb\get($userId, 'user');
}

function get_db_password($userId) {
    return \IniFileDb\get($userId, 'password');
}

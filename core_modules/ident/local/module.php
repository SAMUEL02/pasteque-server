<?php
//    Pastèque Web back office, local ident module
//
//    Copyright (C) 2015 Scil (http://scil.coop)
//
//    This file is part of Pastèque.
//
//    Pastèque is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pastèque is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pastèque.  If not, see <http://www.gnu.org/licenses/>.

namespace PastequeLocalIdent {
    require_once(dirname(__FILE__) . '/config.php');

    function config() {
        global $config;
        return $config;
    }

    function get_local_auth_database() {
        $config = \PastequeLocalIdent\config();
        return new \PDO($config['db_dsn'], $config['db_username'], $config['db_password']);
    }

    function getUser($login) {
        // Check in database...
        // Get the main database
        $dbh_ident = get_local_auth_database();
        $stmt = $dbh_ident->prepare('SELECT * FROM pasteque_users '
                . 'WHERE can_login AND user_id = :user_id');
        $stmt->bindParam(':user_id', $login, \PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (count($result) != 1) {
            // Bouh, invalid user
            \Pasteque\Log::info(sprintf('Unavailable user %s', $login));
            return null;
        }
        $userDbData = $result[0];
        return array('id' => $login, 'pwd_hash' => $userDbData['password']);
    }
}

namespace Pasteque {

    function core_ident_getUser($login) {
        return \PastequeLocalIdent\getUser($login);
    }

}

